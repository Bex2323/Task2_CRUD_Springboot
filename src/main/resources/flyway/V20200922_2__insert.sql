INSERT INTO fond (id,fond_number,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'+77775556644',1,1,2,3),
(2,'+77775556644',2,1,2,3),
(3,'+77775556644',3,1,2,3);

INSERT INTO company (id,name_ru,name_kz,name_en,BIN,parent_id,fond_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'ааа','аб',',iia','adf',1,2,3,4,5,6),
(2,'ббб','ба',',aa','ad',1,2,3,4,5,6),
(3,'ссс','бс',',aa','adf',1,2,3,4,5,6);

INSERT INTO company_unit (id,name_ru,name_kz,name_en,parent_id,year,company_id,code_index,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'ааа','аб',',bim',1,2,3,'123',4,5,6,7),
(2,'ббб','ба',',bim',1,2,3,'123',4,5,6,7),
(3,'ссс','бс',',bim',1,2,3,'123',4,5,6,7);

INSERT INTO Users (id,auth_id,name,fullname,surname,secondname,status,company_unit_id,password,last_login_timestamp,iin,is_active,is_activated,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,11,'sam','sam1','sam2','sam3','sam4',2,'sam5',3,'sam6',true,true,4,5,6,7),
(2,12,'san','san1','san2','san3','san4',2,'san5',3,'san6',true,true,4,5,6,7),
(3,13,'sas','sas1','sas1','sas3','sas4',2,'sas5',3,'sas6',true,true,4,5,6,7);

INSERT INTO authentication (id,username,email,password,role,forgot_password_key,forgot_password_key_timestamp,company_unit_id)
VALUES
(1,'sam','sam1','sam2','sam3','sam4',1,2),
(2,'san','san1','san2','san3','san4',1,2),
(3,'sas','sas1','sas1','sas3','sas4',1,2);

INSERT INTO record (id,number,type,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'tor1','tor2',1,2,3,4,5),
(2,'loki1','loki2',1,2,3,4,5),
(3,'odin1','odin2',1,2,3,4,5);

INSERT INTO case_index (id,case_index,title_ru,title_kz,title_en,storage_type,storage_year,note,company_unit_id,nomenclature_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'tor1','tor2','tor3','tor4',1,2,'tor5',3,4,5,6,7,8),
(2,'loki1','loki2','loki3','loki4',1,2,'loki5',3,4,5,6,7,8),
(3,'odin1','odin2','odin3','odin4',1,2,'odin5',3,4,5,6,7,8);

INSERT INTO Nomenclature(id,nomenclature_number,year,nomenclatue_summary_id,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'35',1,1,3,4,5,6,7),
(2,'27',1,3,3,4,5,6,7),
(3,'21',1,4,3,4,5,6,7);


INSERT INTO Cases(id,case_number, number_of_Tom, case_heading_ru, case_heading_kz, case_heading_en, start_date, end_date,nubmer_page,ESP,ESP_sign,NAF, deleting, access, hash, version_activation,note, id_location, case_index_id, id_record, company_unit_id, act_id,blockchain, date_add_blockchain, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES
(1,'bim1','bim2','бим','бим',',bim',1,2,3,true,'bim3',true,true,true,'bim4',TRUE,'bim5',5,6,7,8,9,'bim7',10,12,13,14,15),
(2,'bim1','bim2','бим','бим',',bim',1,2,3,true,'bim3',true,true,true,'bim4',TRUE,'bim5',5,6,7,8,9,'bim7',10,12,13,14,15),
(3,'bim1','bim2','бим','бим',',bim',1,2,3,true,'bim3',true,true,true,'bim4',TRUE,'bim5',5,6,7,8,9,'bim7',10,12,13,14,15);

INSERT INTO Request(id,request_user_id,response_user_id,case_id,case_index_id,created_type,comment,status,timestamp,sharestart,sharefinish,favorite,updated_timestamp,updated_by,declinenote,company_unit_id,from_request_id)
VALUES
(1,1,2,3,4,'GGG1','GGG2','GGG3',5,6,7,true,8,9,'GGG4',10,11),
(2,1,2,3,4,'JJJ1','JJJ2','JJJ3',5,6,7,true,8,9,'JJJ5',10,11),
(3,1,2,3,4,'FFF1','FFF2','FFF3',5,6,7,true,8,9,'FFF6',10,11);

INSERT INTO share (id,request_id,note,sender_id,receiver_id,share_timestamp)
VALUES
(1,1,'GGG1',2,3,4),
(2,1,'JJJ1',2,3,4),
(3,1,'FFF1',2,3,4);


INSERT INTO history_request(id,request_id, status, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES
(1,1,'status1',1,1,1,1),
(2,2,'status2',2,2,2,2),
(3,3,'status3',3,3,3,3);

INSERT INTO act(id,number_act,base,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES
(1,'act1','base1',1,1,1,1,1),
(2,'act2','base2',2,2,2,2,2),
(3,'act3','base3',3,3,3,3,3);

INSERT INTO Nomenclature_summary(id,number,year,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES  (1,'num1',1,1,1,1,1,1),
        (2,'num2',2,2,2,2,2,2),
        (3,'num3',3,3,3,3,3,3);

INSERT INTO Catalog_case(id,case_id, catolog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,1,1,1,1,1,1,1),
       (2,2,2,2,2,2,2,2),
       (3,3,3,3,3,3,3,3);

INSERT INTO search_key_routing(id,search_key_id, table_name, table_id, type)
VALUES (1,1,'name1',1,'type1'),
       (2,2,'name2',2,'type2'),
       (3,3,'name3',3,'type3');

INSERT INTO search_key(id,name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,'name1',1,1,1,1,1),
       (2,'name2',2,2,2,2,2),
       (3,'name3',3,3,3,3,3);

INSERT INTO File(id,name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,'name1','type1',1,1,'hash1',true,3,1,1,1,1),
       (2,'name2','type2',2,2,'hash2',false,2,2,2,2,2),
       (3,'name3','type3',3,3,'hash1',true,3,3,3,3,3);

INSERT INTO Tempfiles(id,file_binary, file_binary_byte)
VALUES (1,'as1','ds'),
       (2,'as2','ds'),
       (3,'as3','sd');

INSERT INTO File_routing(id,file_id, table_name, table_id, type)
VALUES (1,1,'nameT1',1,'type1'),
       (2,2,'nameT2',2,'type2'),
       (3,3,'nameT3',3,'type3');

INSERT INTO locations (id,row, line,columns,box,company_unit_id,created_timestamp,created_by,updated_timestamp,updated_by)
VALUES (1,'row1','line1','colum1','box1',1,1,1,1,1),
       (2,'row2','line2','colum2','box2',2,2,2,2,2),
       (3,'row3','line3','colum3','box3',3,3,3,3,3);

INSERT INTO notification(id,object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id)
VALUES (1,'BBB',1,1,1,1,1,TRUE ,'TITLE1','TEXT1',1),
       (2,'ZZZ',2,2,2,2,2,TRUE ,'TITLE2','TEXT2',2),
       (3,'KKK',3,3,3,3,3,TRUE ,'TITLE3','TEXT3',3);

INSERT INTO Catalog(id,name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,'nameru1','namekz1','nameen1',1,1,1,1,1,1),
       (2,'nameru2','namekz2','nameen2',2,2,2,2,2,2),
       (3,'nameru3','namekz3','nameen3',3,3,3,3,3,3);

INSERT INTO Activity_journal(id,type, object_type, object_id, created_timestamp, created_by, message_level, message)
VALUES (1,'type1','ob1',1,1,1,'messlevel1','mess1'),
       (2,'type2','ob2',2,2,2,'messlevel2','mess2'),
       (3,'type3','ob3',3,3,3,'messlevel3','mess3');
