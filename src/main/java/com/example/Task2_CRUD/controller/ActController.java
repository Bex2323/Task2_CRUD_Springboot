package com.example.Task2_CRUD.controller;

import com.example.Task2_CRUD.model.Act;
import com.example.Task2_CRUD.service.ActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActController {
    private final ActService actService;

    public ActController(ActService actService) {
        this.actService = actService;
    }

    @GetMapping("/api/Act")
    public ResponseEntity<?> getAct(){
        return ResponseEntity.ok(actService.getAll());
    }

    @GetMapping("/api/Act/{id}")
    public ResponseEntity<?> getAct(@PathVariable Long id){
        return ResponseEntity.ok(actService.getById(id));
    }

    @PostMapping("/api/Act")
    public ResponseEntity<?>  saveAct(@RequestBody Act act){
        return ResponseEntity.ok(actService.create(act));
    }

    @PutMapping("/api/Act")
    public ResponseEntity<?> updateAct(@RequestBody Act act){
        return ResponseEntity.ok(actService.update(act));
    }

    @DeleteMapping("/api/Act/{id}")
    public void deleteAct(@PathVariable Long id) {
        actService.delete(id);
    }
}


