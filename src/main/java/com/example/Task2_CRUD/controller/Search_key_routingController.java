package com.example.Task2_CRUD.controller;

import com.example.Task2_CRUD.model.Search_key_routing;
import com.example.Task2_CRUD.service.Search_key_routingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Search_key_routingController {
    private final Search_key_routingService searchKeyRoutingServiceService;

    public Search_key_routingController(Search_key_routingService searchKeyRoutingServiceService) {
        this.searchKeyRoutingServiceService = searchKeyRoutingServiceService;
    }

    @GetMapping("/api/Search_key_routing")
    public ResponseEntity<?> getSearch_key_routing(){
        return ResponseEntity.ok(searchKeyRoutingServiceService.getAll());
    }

    @GetMapping("/api/Search_key_routing/{id}")
    public ResponseEntity<?> getSearch_key_routing(@PathVariable Long id){
        return ResponseEntity.ok(searchKeyRoutingServiceService.getById(id));
    }

    @PostMapping("/api/Search_key_routing")
    public ResponseEntity<?>  saveSearch_key_routing(@RequestBody Search_key_routing searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingServiceService.create(searchKeyRouting));
    }

    @PutMapping("/api/Search_key_routing")
    public ResponseEntity<?> updateSearch_key_routing(@RequestBody Search_key_routing searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingServiceService.update(searchKeyRouting));
    }

    @DeleteMapping("/api/Search_key_routing/{id}")
    public void deleteSearch_key_routing(@PathVariable Long id) {
        searchKeyRoutingServiceService.delete(id);
    }
}

