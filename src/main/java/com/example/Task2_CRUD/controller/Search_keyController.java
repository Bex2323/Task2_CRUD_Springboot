package com.example.Task2_CRUD.controller;

import com.example.Task2_CRUD.model.Search_key;
import com.example.Task2_CRUD.service.Search_keyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Search_keyController {
    private final Search_keyService searchkeyService;

    public Search_keyController(Search_keyService searchkeyService) {
        this.searchkeyService = searchkeyService;
    }

    @GetMapping("/api/Searchkey")
    public ResponseEntity<?> getSearchkey(){
        return ResponseEntity.ok(searchkeyService.getAll());
    }

    @GetMapping("/api/Searchkey/{id}")
    public ResponseEntity<?> getSearchkey(@PathVariable Long id){
        return ResponseEntity.ok(searchkeyService.getById(id));
    }

    @PostMapping("/api/Searchkey")
    public ResponseEntity<?>  saveSearchkey(@RequestBody Search_key searchkey){
        return ResponseEntity.ok(searchkeyService.create(searchkey));
    }

    @PutMapping("/api/Searchkey")
    public ResponseEntity<?> updateSearchkey(@RequestBody Search_key searchkey){
        return ResponseEntity.ok(searchkeyService.update(searchkey));
    }

    @DeleteMapping("/api/Searchkey/{id}")
    public void deleteSearchkey(@PathVariable Long id) {
        searchkeyService.delete(id);
    }
}
