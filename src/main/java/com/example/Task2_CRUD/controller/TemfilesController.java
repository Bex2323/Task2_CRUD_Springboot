package com.example.Task2_CRUD.controller;

import com.example.Task2_CRUD.model.Temfiles;
import com.example.Task2_CRUD.service.TemfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TemfilesController {
    private final TemfilesService temfilesService;

    public TemfilesController(TemfilesService temfilesService) {
        this.temfilesService = temfilesService;
    }

    @GetMapping("/api/Tepfiles")
    public ResponseEntity<?> getTemfiles(){
        return ResponseEntity.ok(temfilesService.getAll());
    }

    @GetMapping("/api/Tepfiles/{id}")
    public ResponseEntity<?> getTemfiles(@PathVariable Long id){
        return ResponseEntity.ok(temfilesService.getById(id));
    }

    @PostMapping("/api/Tepfiles")
    public ResponseEntity<?>  saveTemfiles(@RequestBody Temfiles temfiles){
        return ResponseEntity.ok(temfilesService.create(temfiles));
    }

    @PutMapping("/api/Tepfiles")
    public ResponseEntity<?> updateTemfiles(@RequestBody Temfiles temfiles){
        return ResponseEntity.ok(temfilesService.update(temfiles));
    }

    @DeleteMapping("/api/Tepfiles/{id}")
    public void deleteTemfiles(@PathVariable Long id) {
        temfilesService.delete(id);
    }
}

